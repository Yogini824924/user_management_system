const express = require("express");
const cors = require("cors");
const session = require('express-session');

const app = express();

app.use(cors());

app.use(session({
    secret: '123456cat',
    resave: false,
    saveUninitialized: true,
    cookie: { maxAge: 60000 }
}));


const db = require("./app/models");

db.sequelize.sync();
// // drop the table if it already exists
// db.sequelize.sync({ force: true }).then(() => {
//   console.log("Drop and re-sync db.");
// });

app.use(express.json());
app.use(express.urlencoded({ extended: false }));


// simple route
app.get("/", (request, response) => {
  response.json({ message: "Welcome to user management system." });
});

require("./app/routes/user.routes")(app);
require("./app/routes/admin.routes")(app);
app.use((req, res, next) => {
    const error = new Error('Page Not found');
    error.status = 404;
    next(error);
});

app.use((err, req, res, next) => {
    res.locals.message = err.message;
    res.locals.error = err;

    res.status(err.status || 500).json({
        err: true,
        message: err.message
    }).end();
});
// set port, listen for requests
const PORT = process.env.PORT || 8080;

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});