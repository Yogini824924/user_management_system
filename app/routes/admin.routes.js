module.exports = app => {
    const admin = require("../controllers/admin.controller.js");
  
    var router = require("express").Router();
    const db = require('../models');
    const jwt = require('jsonwebtoken');
    const auth = require('./auth.js');

    const User = db.users;
    const Op = db.Sequelize.Op;
  
    // Create a new user
    router.post("/register", admin.create);

    router.post("/login", (request, response) => {
        if(!request.body.email || !request.body.password) {
            response.status(404).json({
                message: 'All fields are required!'
            });
        }
        else {
            let email = request.body.email;
            let password = request.body.password;
            User.findOne({ where: { email, password } })
              .then((data) => {
                  if(data) {
                      const token = jwt.sign({
                          email, password
                      },process.env.SECRET);

                      response.json({ 
                          firstName: data.firstName,
                          lastName: data.lastName,
                          email,
                        token });
                      
                  }
                  else {
                    response.status(404).json({
                        err:true,
                        message: 'Invalid email or password!'
                    });

                  }
              })
              .catch((err) => {
                response.status(404).json({
                    message: 'Some error while logging in!'
                });
              });
        }

    });

  
    // Retrieve all users
    router.get("/",auth, admin.findAll);
  
    // Retrieve a single user with id
    router.get("/:id", admin.findOne);
  
    // Update a user with id
    router.put("/:id", admin.update);
  
    // Delete a User with id
    router.delete("/:id", admin.delete);
  
    // Delete all Users
    router.delete("/", admin.deleteAll);
  
    app.use('/admin', router);
  };