module.exports = app => {
    const users = require("../controllers/user.controller.js");
  
    var router = require("express").Router();
  
    // Create a new user
    router.post("/register", users.create);
  
    // Update a user with id
    router.put("/forgotPassword", users.update);
  
    app.use('/users', router);
  };