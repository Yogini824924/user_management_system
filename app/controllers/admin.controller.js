const db = require('../models');

const User = db.users;
const Op = db.Sequelize.Op;

// Create and save user
exports.create = (request, response) => {
  // validate request
  console.log(request.body);
  if(!request.body.firstName || !request.body.lastName || !request.body.email || !request.body.password) {
      response.status(404).json({
          message: 'All fields are required!'
      });
      return;
  }
  else {
    if(request.body.password !== request.body.confirmPassword) {
      response.status(404).json({
        message: 'Password not Matched!'
    });
    return;
    } else {
        // create a user
        const user = {
          firstName: request.body.firstName, 
          lastName: request.body.lastName, 
          email: request.body.email, 
          password: request.body.password, 
          admin: request.body.admin ? request.body.admin : false
        };

        // save user in database
        User.create(user)
          .then((data) => {
              response.send(data);
          })
          .catch((err) => {
              response.status(500).send({
                  message: err.message || 'Some error occurred while creating the user.'
              });
          });
    }
  }
};

exports.update = (request, response) => {
    const id = request.params.id;

  User.update(request.body, {
    where: { id: id }
  })
    .then((num) => {
      if (num == 1) {
        response.send({
          message: "User was updated successfully."
        });
      } else {
        response.json({
          message: `Cannot update User with id=${id}. Maybe User was not found or request.body is empty!`
        });
      }
    })
    .catch((err) => {
      response.status(500).json({
        message: `Error updating User with id=${id}`
      });
    });

};

exports.findAll = (request, response) => {
    Tutorial.findAll({where: {
        admin: false
    }})
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving tutorials."
        });
      });
};

  exports.findOne = (request, response) => {
    const id = request.params.id;
  
    Tutorial.findByPk(id)
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message: `Error retrieving user with id=${id}`
        });
      });
};

exports.update = (request, response) => {
    const id = request.params.id;
  
    Tutorial.update(request.body, {
      where: { id: id }
    })
      .then((num) => {
        if (num == 1) {
          res.send({
            message: "User was updated successfully."
          });
        } else {
          res.send({
            message: `Cannot update User with id=${id}. Maybe User was not found or request.body is empty!`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: `Error updating User with id=${id}`
        });
      });
  };

exports.delete = (request, response) => {
    const id = request.params.id;

    User.destroy({
      where: { id: id }
    })
      .then((num) => {
        if (num == 1) {
          response.send({
            message: "User was deleted successfully!"
          });
        } else {
          response.send({
            message: `Cannot delete User with id=${id}. Maybe User was not found!`
          });
        }
      })
      .catch((err) => {
        response.status(500).json({
          message: `Could not delete User with id=${id}`
        });
      });
};

exports.deleteAll = (request, response) => {
    User.destroy({
      where: {},
      truncate: false
    })
      .then((nums) => {
        response.send({ message: `${nums} users were deleted successfully!` });
      })
      .catch(err => {
        response.status(500).send({
          message:
            err.message || "Some error occurred while removing all tutorials."
        });
      });
};
