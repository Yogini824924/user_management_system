require('dotenv').config();

module.exports = {
    mysqlHost: process.env.MYSQL_HOST, 
    mysqlUsername: process.env.MYSQL_USERNAME, 
    mysqlPassword: process.env.MYSQL_PASSWORD, 
    mysqlDatabase: process.env.MYSQL_DATABASE, 
    dialect: 'mysql',
    pool: {
        max: 5, 
        min: 0, 
        acquire: 30000, 
        idle: 10000
    }
};